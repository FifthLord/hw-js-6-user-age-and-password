

function createNewUser() {
   const newUser = {};

   Object.defineProperties(newUser, {
      _firstName: {
         value: prompt("Введіть ваше Ім'я"),
         writable: false,
         configurable: true
      },
      _lastName: {
         value: prompt("Введіть ваше Прізвище"),
         writable: false,
         configurable: true
      }
   });

   newUser.birthday = new Date((prompt("Введіть вашу дату народження", "день.місяць.рік")).split('.').reverse().join("."));
   newUser.getAge = function () {
      return ((new Date().getTime() - this.birthday) / (24 * 3600 * 365.25 * 1000)) | 0
   }

   newUser.getLogin = function () {
      return ((this._firstName[0] + this._lastName).toLowerCase());
   }

   newUser.getPassword = function () {
      return (this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this.birthday.getFullYear());
   }

   newUser.setFirstName = function (newFirstName) {
      Object.defineProperty(newUser, '_firstName', {
         value: newFirstName,
      })
   }
   newUser.setLastName = function (newLastName) {
      Object.defineProperty(newUser, '_lastName', {
         value: newLastName,
      })
   }
   console.log(newUser); //вивожу тут, щоб не вводити вда рази дату
   return newUser;
}

// const newUser = Object.defineProperties({}, Object.getOwnPropertyDescriptors(createNewUser()));
const newUser = createNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());


//-------TEST--------//

// newUser._firstName = "TEST1_1";
// newUser._lastName = "TEST2_1";
// console.log(newUser);

// newUser.setFirstName("TEST1_2");
// newUser.setLastName("TEST2_2");
// console.log(newUser);